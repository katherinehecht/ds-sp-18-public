// set.cpp

#include <algorithm>
#include <iostream>
#include <set>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    set<string> names = {"Jack", "Jane", "Meg", "Jack", "Jill"}; // creating a set

    /*
    // alternative inserting values into a set
    names.insert("Jack");
    names.insert("Jane");
    names.insert("Meg");
    names.insert("Jack");
    names.insert("Jill");
    */

    //Note: duplicate entries cause just 1 entry into the set
    cout << "Size:            " << names.size() << endl;

    // traversing a set - using fancy for_each notation.
    cout << "Elements:       ";
    for_each(names.begin(), names.end(), [](string n){cout << " " << n;});
    cout << endl;

    // searching in a set
    cout << "Search(Jill):   "
    	 << (names.find("Jill") != names.end() ? "Found" : "Not Found") << endl;

    cout << "Search(Plort): "
    	 << (names.find("Plort") != names.end() ? "Found" : "Not Found") << endl;

    return 0;
}
