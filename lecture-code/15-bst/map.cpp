// map.cpp

#include <algorithm>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    map<string, string> heroes = {	// creating entries in a map
    {"severus" , "x-slytherin"},
    {"hermione", "b-gryffindor"},
	{"luna"    , "a-ravenclaw"},
	};

    cout << "Size:               " << heroes.size() << endl;

    // traversing a map using fancy for (auto p : map_name)
    cout << "Elements:          ";
    for (auto p : heroes) {
    	cout << " (" << p.first << ", " << p.second << ")";
    }
    cout << endl;

    //searching using keys in a map.
    cout << "Search(hermione):   "
    	 << (heroes.find("hermione") != heroes.end() ? "Found" : "Not Found") << endl;

    cout << "Search(voldemort): "
    	 << (heroes.find("voldemort") != heroes.end() ? "Found" : "Not Found") << endl;

    cout << "Current value for severus: ";
    cout << heroes["severus"] << endl; // using an entry from a map with a key
    heroes["severus"] = "Order of the Phoenix"; // updating a value for a key
    cout << "Updated value for severus: ";
    cout << heroes["severus"] << endl; // accessing updated map entry with key

    //Note: searching using values in a map is quite impractical, though not impossible. We typically use a map to check - what is the value associated with this key.
    //To search for a value. Basically, check all entries in a map to see if a value matches the target value.

    return 0;
}
