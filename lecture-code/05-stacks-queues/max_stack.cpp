// max_stack.cpp

#include "max_stack.hpp"

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    max_stack<int> s;

    s.push(2);
    cout << s.max() << endl;
    s.push(3);
    cout << s.max() << endl;
    s.push(1);
    cout << s.max() << endl;
    s.pop();
    cout << s.max() << endl;
    s.pop();
    cout << s.max() << endl;

    return 0;
}
